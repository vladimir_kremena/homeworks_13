package ua.ithillel.dnipro.kremena.homework.utils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class FileUtil {

    public static List<String> read(String path) {
        List<String> list = new ArrayList<>();
        try(BufferedReader reader = new BufferedReader(new FileReader(new File(path)))) {
            while(true) {
                String line = reader.readLine();
                if (line == null) break;
                list.add(line);
            }
            return list;
        } catch (IOException iOE) {
            iOE.printStackTrace();
            return null;
        }
    }

    public synchronized static void write(String path, String title, Map<String, List<String>> headers) {
        File file = new File(path);
        createDirectoryAndFile(file);
        try(BufferedWriter writer = new BufferedWriter(new FileWriter(file, true))) {
            writer.write(title + "\n");
            writer.write("-".repeat(40) + "\n");
            for (Map.Entry<String, List<String>> header : headers.entrySet()) {
                writer.write(header.getKey() + ": " + header.getValue() + "\n");
            }
            writer.write("-".repeat(80) + "\n");
            writer.flush();
        } catch (IOException iOE) {
            iOE.printStackTrace();
        }
    }

    private static void createDirectoryAndFile(File file) {
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException iOE) {
                System.out.println("File creation error: " + iOE.getMessage());
            }
        }
    }
}