package ua.ithillel.dnipro.kremena.homework.utils;

import lombok.RequiredArgsConstructor;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

@RequiredArgsConstructor
public class AppProperties {

    private final String pathToConfigFile;

    public String getProperty(String property) {
        Properties properties = new Properties();
        try {
            properties.load(new FileReader(new File(pathToConfigFile)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return String.valueOf(properties.get(property));
    }
}