package ua.ithillel.dnipro.kremena.homework;

import ua.ithillel.dnipro.kremena.homework.http.CustomHttpClient;
import ua.ithillel.dnipro.kremena.homework.utils.AppProperties;
import ua.ithillel.dnipro.kremena.homework.utils.FileUtil;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class AppBuilder {

    public static final int qtyCoreProcessor = Runtime.getRuntime().availableProcessors();
    public static final String pathToConfigFile = "custom_config.properties";

    public static void main(String[] args) {
        AppProperties properties = new AppProperties(pathToConfigFile);
        String pathToSitesFile = properties.getProperty("path.to.sites.file");
        String pathToOutFile = properties.getProperty("path.to.out.file");
        List<String> sites = FileUtil.read(pathToSitesFile);
        CustomHttpClient httpClient = new CustomHttpClient();
        ExecutorService executor = Executors.newFixedThreadPool(qtyCoreProcessor);

        for (String site : sites) {
            executor.submit(() -> {
                FileUtil.write(pathToOutFile, site, httpClient.getHeaders(site));
            });
        }

        try {
            executor.shutdown();
        } catch (SecurityException sE) {
            executor.shutdownNow();
        }

        try {
            executor.awaitTermination(30, TimeUnit.SECONDS);
            if (executor.isTerminated()) {
                System.out.println("All threads have completed their execution");
            } else {
                System.out.println("Not all threads have completed their execution");
                executor.shutdownNow();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}